print("hello!")
import pandas as pd
import numpy as np
#import matplotlib
import matplotlib.pyplot as plt
import xgboost as xgb
import ROOT
#from ROOT import *
import scipy.integrate as integrate
import scipy.special as special

from BDT_all_1 import s1  
from BDT_all_1 import s2, s3, s4, s5, s6, s7, s8, s9, sr

#REMEMBER TO ALSO CHANGE IT IN RENAME.PY
s=sr
print(s)

import rename

#depth of tree:
d=7

#rename


print("Extracting training data and converting it to numpy object")
df_train = ROOT.RDataFrame("Events", "train_allxbg2_*_{}.root".format(s))
npy_train = df_train.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train = pd.DataFrame(npy_train)



#df_train_ttbb = ROOT.RDataFrame("Events", "train_allxbg_ttbb_{}.root".format(s))
#npy_train_ttbb = df_train_ttbb.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label"])
#panda_all_trainttbb = pd.DataFrame(npy_train_ttbb)


df_train_R43 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2R_45_300.root")
npy_trainR43 = df_train_R43.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_trainR43 = pd.DataFrame(npy_trainR43)


df_train_43 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_300.root")
npy_train43 = df_train_43.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train43 = pd.DataFrame(npy_train43)

df_train_44 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_400.root")
npy_train44 = df_train_44.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train44 = pd.DataFrame(npy_train44)

df_train_45 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_500.root")
npy_train45 = df_train_45.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train45 = pd.DataFrame(npy_train45)

df_train_46 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_600.root")
npy_train46 = df_train_46.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train46 = pd.DataFrame(npy_train46)

df_train_47 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_700.root")
npy_train47 = df_train_47.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train47 = pd.DataFrame(npy_train47)

df_train_48 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_800.root")
npy_train48 = df_train_48.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train48 = pd.DataFrame(npy_train48)

df_train_49 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_900.root")
npy_train49 = df_train_49.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train49 = pd.DataFrame(npy_train49)

df_train_410 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_1000.root")
npy_train410 = df_train_410.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train410 = pd.DataFrame(npy_train410)

df_train_411 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_1100.root")
npy_train411 = df_train_411.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train411 = pd.DataFrame(npy_train411)

df_train_412 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_45_1200.root")
npy_train412 = df_train_412.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train412 = pd.DataFrame(npy_train412)

df_train_33 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_300.root")
npy_train33 = df_train_33.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train33 = pd.DataFrame(npy_train33)

df_train_34 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_400.root")
npy_train34 = df_train_34.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train34 = pd.DataFrame(npy_train34)

df_train_35 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_500.root")
npy_train35 = df_train_35.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train35 = pd.DataFrame(npy_train35)
"""
"""
df_train_13 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_15_300.root")
npy_train13 = df_train_13.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train13 = pd.DataFrame(npy_train13)

df_train_14 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_15_400.root")
npy_train14 = df_train_14.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train14 = pd.DataFrame(npy_train14)

df_train_15 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_15_500.root")
npy_train15 = df_train_15.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train15 = pd.DataFrame(npy_train15)

df_train_16 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_15_600.root")
npy_train16 = df_train_16.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train16 = pd.DataFrame(npy_train16)

df_train_17 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_15_700.root")
npy_train17 = df_train_17.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train17 = pd.DataFrame(npy_train17)

df_train_18 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_15_800.root")
npy_train18 = df_train_18.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train18 = pd.DataFrame(npy_train18)

df_train_23 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_300.root")
npy_train23 = df_train_23.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train23 = pd.DataFrame(npy_train23)

df_train_24 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_400.root")
npy_train24 = df_train_24.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train24 = pd.DataFrame(npy_train24)

df_train_25 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_500.root")
npy_train25 = df_train_25.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train25 = pd.DataFrame(npy_train25)

df_train_26 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_600.root")
npy_train26 = df_train_26.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train26 = pd.DataFrame(npy_train26)

df_train_27 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_700.root")
npy_train27 = df_train_27.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train27 = pd.DataFrame(npy_train27)

df_train_28 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_800.root")
npy_train28 = df_train_28.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train28 = pd.DataFrame(npy_train28)

df_train_29 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_900.root")
npy_train29 = df_train_29.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train29 = pd.DataFrame(npy_train29)

df_train_210 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_25_1000.root")
npy_train210 = df_train_210.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train210 = pd.DataFrame(npy_train210)

df_train_33 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_300.root")
npy_train33 = df_train_33.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train33 = pd.DataFrame(npy_train33)

df_train_34 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_400.root")
npy_train34 = df_train_34.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train34 = pd.DataFrame(npy_train34)

df_train_35 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_500.root")
npy_train35 = df_train_35.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train35 = pd.DataFrame(npy_train35)

df_train_36 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_600.root")
npy_train36 = df_train_36.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train36 = pd.DataFrame(npy_train36)

df_train_37 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_700.root")
npy_train37 = df_train_37.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train37 = pd.DataFrame(npy_train37)

df_train_38 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_800.root")
npy_train38 = df_train_38.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train38 = pd.DataFrame(npy_train38)

df_train_39 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_900.root")
npy_train39 = df_train_39.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train39 = pd.DataFrame(npy_train39)

df_train_310 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_1000.root")
npy_train310 = df_train_310.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train310 = pd.DataFrame(npy_train310)

df_train_311 = ROOT.RDataFrame("Events", "train_allx2_sig_SU2L_35_1100.root")
npy_train311 = df_train_311.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
panda_all_train311 = pd.DataFrame(npy_train311)

#panda_bg_train = panda_bg_train1[:40000]



#Concatenate signal and background data
panda_all_train = pd.concat([panda_all_train, panda_all_trainR43])
#panda_all_train = pd.concat([panda_all_train, panda_all_train33, panda_all_train34, panda_all_train35, panda_all_train36, panda_all_train37, panda_all_train38, panda_all_train39, panda_all_train310, panda_all_train311, panda_all_train43, panda_all_train44, panda_all_train45, panda_all_train46, panda_all_train47, panda_all_train48, panda_all_train49, panda_all_train410, panda_all_train411, panda_all_train412, panda_all_train23, panda_all_train24, panda_all_train25, panda_all_train26, panda_all_train27, panda_all_train28, panda_all_train29, panda_all_train210, panda_all_train13, panda_all_train14, panda_all_train15, panda_all_train16, panda_all_train17, panda_all_train18])

print(panda_all_train.shape)

#Shuffle the concatenated frames so that signal and background are mixed
panda_train = panda_all_train.sample(frac=1)



#add 'Label' as category to pandas frames
panda_train['Label'] = panda_train.Label.astype('category')
print('Fraction signal in training data: {}'.format(len(panda_train[panda_train.Label == 1])/(float)(len(panda_train[panda_train.Label == 1]) + len(panda_train[panda_train.Label == 0]))))

fr = 1/(len(panda_train[panda_train.Label == 1])/(float)(len(panda_train[panda_train.Label == 1]) + len(panda_train[panda_train.Label == 0])))
print(fr)

#panda_test['Label'] = panda_test.Label.astype('category')
panda_train['Label'] = panda_train.Label.astype('category')



print("length of panda train: ",len(panda_train))


print("create DMatrices")
#Create xgb DMatrices
feature_names = panda_train.columns[0:8]
#print(panda_train[feature_names])
train = xgb.DMatrix(data=panda_train[feature_names], label=panda_train.Label.cat.codes, feature_names=feature_names)
print("DMatrix created!")



"""
import seaborn as sn

#Correlation matrix
corr_matrix = panda_test[feature_names].corr()
print(corr_matrix)
#corr_matrix.to_csv('correlationmatrix.csv')
plt.figure(figsize=(16,10))
hm = sn.heatmap(corr_matrix,vmin=-1, vmax=1, center=0, cmap=sn.diverging_palette(10, 230, s=100, l=50, n=256), annot=True)
plt.show()
figure = hm.get_figure()
font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 6}
matplotlib.rc('font', **font)
figure.savefig("corr_matrix_heatmap_{}_new_testpleasedisregard.png".format(s), bbox_inches="tight",dpi=500)
#.style.background_gradient(cmap='Blues')\
"""

param = {}

# Booster parameters
param['eta']              = 0.1 # learning rate
param['max_depth']        = d  # maximum depth of a tree
#param['subsample']        = 0.8 # fraction of events to train tree on
#param['colsample_bytree'] = 0.8 # fraction of features to train tree on
param['scale_pos_weight'] = fr
#param['tree_method'] 	  ='gpu_hist'

# Learning task parameters
param['objective']   = 'binary:logistic' # objective function
param['eval_metric'] = 'error'           # evaluation metric for cross validation
param = list(param.items()) + [('eval_metric', 'logloss')] + [('eval_metric', 'rmse')]

num_trees = 100  # number of trees to make


#training:
print("training...")
booster = xgb.train(param,train,num_boost_round=num_trees)

#booster.save_model("{}_model_v3_ttbg_weighted.txt".format(s))
booster.save_model("{}_model_v3_p2.json".format(s))
#booster.save_model("{}_model_v3_ttbg.ubj".format(s))
booster.save_model("{}_model_v3_p2.model".format(s))
print(booster.best_ntree_limit)

#print rmse
#print(booster.eval(test))
#make prediction for test data set
#predictions = booster.predict(test)
#pred_whole = booster.predict(whole)

#write discriminant as branch in whole dataset
#def discriminant_branch(df):
#    return df.Define("discriminant", pred_whole)

#xgb.plot_tree(booster, num_tree = 0)

"""
print("plotting discriminant")
# plot signal and background separately
plt.figure();
plt.hist(predictions[~test.get_label().astype(bool)],bins=np.linspace(0,1,50), density=True,         histtype='step',color='midnightblue',label='background');
plt.hist(predictions[(test.get_label().astype(bool))],bins=np.linspace(0,1,50), density=True,         histtype='step',color='firebrick',label='signal');
# make the plot readable
plt.xlabel('Discriminant',fontsize=12);
plt.ylabel('Events',fontsize=12);
plt.legend(frameon=False);
plt.savefig("discriminant_all_{0}_depth{1}_singleS.png".format(s,d))
print("save discriminant")

#np.savetxt("discriminant.txt", predictions)

#ROOT.RDF.MakeNumpyDataFrame(array_data) (Olga's tipp)

# choose score cuts:
cuts = np.linspace(0,1,500);
cuts=np.linspace(0,0.1,500)+np.linspace(0.1,1,50)
nsignal = np.zeros(len(cuts));
nbackground = np.zeros(len(cuts));
for i,cut in enumerate(cuts):
    nsignal[i] = len(np.where(predictions[test.get_label().astype(bool)] > cut)[0]); #true positive
    nbackground[i] = len(np.where(predictions[~(test.get_label().astype(bool))] > cut)[0]); #false positive
    
#print("nsignal: ", nsignal[:-1])
#print("nbackground: ", nbackground[:-1])
print("plotting ROC")    
# plot efficiency vs. purity (ROC curve)
plt.figure();
#plt.plot(1-(nsignal/len(panda_test[panda_test.Label == 0])),1-(nsignal/(nsignal + nbackground)),'o-',color='blueviolet');
plt.plot(nsignal/(nsignal + nbackground),nbackground/(nsignal + nbackground)),'o-',color='blueviolet');

np.seterr(invalid='ignore')
from sklearn.metrics import roc_auc_score
#fpr, tpr, _ = roc_curve(signal, panda_test[panda_test.Label==0])
print("calculating AUC")
score = roc_auc_score(panda_test.Label, predictions)
print(score)
plt.title("{}".format(score))

# make the plot readable
plt.xlabel('Efficiency',fontsize=12);
plt.ylabel('Purity',fontsize=12);
plt.legend(frameon=False);
plt.savefig("ROC_all_{0}_depth{1}_singleS.png".format(s,d))
print("ROC saved")
xgb.plot_importance(booster,grid=False);
plt.savefig("importance_all_{0}_depth{1}_singleS.png".format(s,d), bbox_inches="tight")
print("finished succesfully")

"""
