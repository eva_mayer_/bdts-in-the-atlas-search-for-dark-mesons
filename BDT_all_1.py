sr = "SU2R_45_300"
s1 = "SU2L_25_800"
s2 = "SU2L_45_400"

s3 = "SU2L_25_700"
s4 = "SU2L_25_900"
s5 = "SU2L_15_700"
s6 = "SU2L_15_800"
s7 = "SU2L_35_700"
s8 = "SU2L_35_800"
s9 = "SU2L_35_900"


h="R"

if __name__ == "__main__": 
    print("hi!")
    import pandas as pd
    import numpy as np
    import matplotlib.pyplot as plt
    import xgboost as xgb
    import ROOT
    #from ROOT import *
    print("all modules are imported")



    
#bg_model = xgb.Booster().load_model("model_bg.txt")
#discriminant = bg_model.predict(matrix)


def filter_events(df):
    """
    Reduce initial dataset to only events which shall be used for training
    """
    return df.Filter("BJets_70_N>=3 && Jets_N>=5 && HT_pt>=300", "At least two b jets and four jets and HT >0 300")


def define_variables(df,v):
    """
    Define the variables which shall be used for training
    """
    return df.Define("BJets_70_N_c", "BJets_70_N")\
             .Define("Jets_pt_0_c", "Jets_pt_0")\
    	     .Define("bb_m_for_minDeltaR_c", "bb_m_for_minDeltaR")\
	     .Define("HT_pt_c", "HT_pt")\
	     .Define("Jets_N_c", "Jets_N")\
             .Define("Leptons_pt_c", "Leptons_pt")\
             .Define("deltaRBJet1Lep_c", "deltaRBJet1Lep")\
             .Define("deltaRLep2ndClosestBJet_c", "deltaRLep2ndClosestBJet")\
             .Define("minDeltaRBJets_c", "minDeltaRBJets")\
             .Define("LJetPlusClosestBJet_8_m_c", "LJetPlusClosestBJet_8_m")\
             .Define("LJetPlusClosestBJet_12_m_c", "LJetPlusClosestBJet_12_m")\
             .Define("LJet_m_plus_RCJet_m_8_c", "LJet_m_plus_RCJet_m_8")\
             .Define("LJet_m_plus_RCJet_m_12_c", "LJet_m_plus_RCJet_m_12")\
             .Define("mT_c", "mT")\
             .Define("MET_pt_c", "MET_pt")\
	     .Define("METSig__c", "METSig_")\
             .Define("ljet_m_c", "ljet_m")\
             .Define("LJets_8_m_c", "LJets_8_m")\
             .Define("LJets_12_m_c", "LJets_12_m")\
             .Define("min_m_bb_c", "min_m_bb")\
             .Define("jet_isbtagged_DL1r_70_c", "jet_isbtagged_DL1r_70")\
    	     .Define("Label", "1")\
    	     .Define("exactlabel", "string(v)")\
             .Define("event", "(int) rdfentry_")\
             .Define("weight", "weight70")

#.Define("Jets_pt_0_c", "Jets_pt_0")\
#.Define("MET_pt_c", "MET_pt")\
#.Define("METSig__c", "METSig_")\


def define_variables_bg(df):
    return df.Define("BJets_70_N_c", "BJets_70_N")\
    	     .Define("Jets_pt_0_c", "Jets_pt_0")\
    	     .Define("bb_m_for_minDeltaR_c", "bb_m_for_minDeltaR")\
	     .Define("HT_pt_c", "HT_pt")\
	     .Define("Jets_N_c", "Jets_N")\
             .Define("Leptons_pt_c", "Leptons_pt")\
             .Define("deltaRBJet1Lep_c", "deltaRBJet1Lep")\
             .Define("deltaRLep2ndClosestBJet_c", "deltaRLep2ndClosestBJet")\
             .Define("minDeltaRBJets_c", "minDeltaRBJets")\
             .Define("LJetPlusClosestBJet_8_m_c", "LJetPlusClosestBJet_8_m")\
             .Define("LJetPlusClosestBJet_12_m_c", "LJetPlusClosestBJet_12_m")\
             .Define("LJet_m_plus_RCJet_m_8_c", "LJet_m_plus_RCJet_m_8")\
             .Define("LJet_m_plus_RCJet_m_12_c", "LJet_m_plus_RCJet_m_12")\
             .Define("mT_c", "mT")\
             .Define("MET_pt_c", "MET_pt")\
	     .Define("METSig__c", "METSig_")\
             .Define("LJets_8_m_c", "LJets_8_m")\
             .Define("LJets_12_m_c", "LJets_12_m")\
             .Define("min_m_bb_c", "min_m_bb")\
    	     .Define("Label", "0")\
             .Define("event", "(int) rdfentry_")\
             .Define("weight", "weight70")

#.Define("Jets_pt_0_c", "Jets_pt_0")\             
#             .Define("MET_pt_c", "MET_pt")\
#             .Define("METSig__c", "METSig_")\



variables = ["weight", "BJets_70_N_c", "Jets_pt_0_c", "bb_m_for_minDeltaR_c", "Jets_N_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "deltaRLep2ndClosestBJet_c", "minDeltaRBJets_c", "LJetPlusClosestBJet_8_m_c", "LJetPlusClosestBJet_12_m_c", "LJet_m_plus_RCJet_m_8_c", "LJet_m_plus_RCJet_m_12_c", "mT_c", "MET_pt_c", "METSig__c", "LJets_8_m_c", "LJets_12_m_c", "min_m_bb_c"]


""" (this is just for copy-pasteing the variables that I want, in case I want to change the set of variables):
		"Jets_N_c",
		"BJets_70_N_c"
		"Leptons_pt_c",
		"HT_pt_c",
		"Jets_pt_0_c",
		"deltaRBJet1Lep_c",
		"deltaRLep2ndClosestBJet_c",
		"minDeltaRBJets_c",
x		"LJetPlusClosestBJet_8_m_c",
		"LJetPlusClosestBJet_12_m_c",
x		"LJet_m_plus_RCJet_m_8_c",
		"LJet_m_plus_RCJet_m_12_c",
		"mT_c",
		"ljet_m_c",
x		"LJets_8_m_c",
		"LJets_12_m_c",
		"MET_pt_c",
x		"METSig__c",
x		"min_m_bb_c",
		"bb_m_for_minDeltaR_c",
		"jet_isbtagged_DL1r_70_c" 
"""


if __name__ == "__main__":
    #for filename, label in [["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s1), "{}".format(s1)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s2), "{}".format(s2)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s3), "{}".format(s3)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s4), "{}".format(s4)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s5), "{}".format(s5)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s6), "{}".format(s6)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s7), "{}".format(s7)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s8), "{}".format(s8)], ["/eos/user/e/emayer/signalSU2{0}-signal{1}.root".format(h,s9), "{}".format(s9)]]:
    
    Rsig = [
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_15_200.root", "SU2R_15_400"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_15_300.root", "SU2R_15_300"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_15_400.root", "SU2R_15_400"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_15_500.root", "SU2R_15_500"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_25_200.root", "SU2R_25_200"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_25_300.root", "SU2R_25_300"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_25_400.root", "SU2R_25_400"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_25_500.root", "SU2R_25_500"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_35_200.root", "SU2R_35_200"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_35_300.root", "SU2R_35_300"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_35_400.root", "SU2R_35_400"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_35_500.root", "SU2R_35_500"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_35_600.root", "SU2R_35_600"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_45_200.root", "SU2R_45_200"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_45_300.root", "SU2R_45_300"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_45_400.root", "SU2R_45_400"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_45_500.root", "SU2R_45_500"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_45_600.root", "SU2R_45_600"],
    ["/eos/user/e/emayer/signalSU2R-signalSU2R_45_700.root", "SU2R_45_700"]]
    
    Lsig = [
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_15_300.root", "SU2L_15_300"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_15_400.root", "SU2L_15_400"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_15_500.root", "SU2L_15_500"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_15_600.root", "SU2L_15_600"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_15_700.root", "SU2L_15_700"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_15_800.root", "SU2L_15_800"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_300.root", "SU2L_25_300"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_400.root", "SU2L_25_400"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_500.root", "SU2L_25_500"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_600.root", "SU2L_25_600"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_700.root", "SU2L_25_700"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_800.root", "SU2L_25_800"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_900.root", "SU2L_25_900"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_25_1000.root", "SU2L_25_1000"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_300.root", "SU2L_35_300"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_400.root", "SU2L_35_400"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_500.root", "SU2L_35_500"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_600.root", "SU2L_35_600"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_700.root", "SU2L_35_700"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_800.root", "SU2L_35_800"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_900.root", "SU2L_35_900"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_1000.root", "SU2L_35_1000"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_35_1100.root", "SU2L_35_1100"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_300.root", "SU2L_45_300"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_400.root", "SU2L_45_400"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_500.root", "SU2L_45_500"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_600.root", "SU2L_45_600"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_700.root", "SU2L_45_700"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_800.root", "SU2L_45_800"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_900.root", "SU2L_45_900"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_1000.root", "SU2L_45_1000"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_1100.root", "SU2L_45_1100"],
    ["/eos/user/e/emayer/signalSU2L-signalSU2L_45_1200.root", "SU2L_45_1200"]]
    
    for filename, label in Rsig:
        print(">>> Extract the training and testing events for {} from the {} dataset.".format(
            label, filename))
        print(Rsig.index([filename,label]))
        l = Rsig.index([filename,label])
        
        # Load dataset, filter the required events and define the training variables
        filepath = filename
        label = label
        df = ROOT.RDataFrame("nominal", filepath)
        df = filter_events(df)
        df = define_variables(df,l)

        # Book cutflow report
        report = df.Report()

        # Split dataset by event number for training and testing
        columns = ROOT.std.vector["string"](variables)
        data_train = df.Filter("event % 2 == 0", "Select events with even event number for training")\
                       .Snapshot("Events", "train_allx2_sig_" + label + ".root")
        data_test = df.Filter("event % 2 == 1", "Select events with odd event number for training")\
                      .Snapshot("Events", "test_allx2_sig_" + label + ".root")
        
        	

        # Print cutflow report
        report.Print()
        #print("columns: ", columns)

if __name__ == "__main__":        
    for filename, label in [["/eos/user/e/emayer/ttbb-111122.root", "ttbb"], ["/eos/user/e/emayer/ttbar_allhad-111122.root", "ttallhad"], ["/eos/user/e/emayer/4top-111122.root", "4top"], ["/eos/user/e/emayer/Multiboson-111122.root", "multiboson"], ["/eos/user/e/emayer/SingleTop-111122.root", "singletop"], ["/eos/user/e/emayer/ttbar_nonallhad-111122.root", "ttnonallhad"], ["/eos/user/e/emayer/ttbarPlusX-111122.root", "ttPlusX"], ["/eos/user/e/emayer/VJets-111122.root", "VJets"]]:  
        print(">>> Extract the training and testing events for {} from the {} dataset.".format(label, filename))
        filepath = filename
	
        df = ROOT.RDataFrame("nominal", filepath)
        df = filter_events(df)
        df = define_variables_bg(df)

        # Book cutflow report
        report = df.Report()

        # Split dataset by event number for training and testing
        columns = ROOT.std.vector["string"](variables)
        data_train = df.Filter("event % 2 == 0", "Even event number (training)")\
                       .Snapshot("Events", "train_allxbg2_" + label + "_.root")
        data_test = df.Filter("event % 2 == 1", "Odd event number (test)")\
                      .Snapshot("Events", "test_allxbg2_" + label + "_.root")
        
        	

        # Print cutflow report
        report.Print()
       
