print("hello!")
import pandas as pd
import numpy as np
#import matplotlib
import matplotlib.pyplot as plt
import xgboost as xgb
print("start importing ROOT")
import ROOT
print("done importing ROOT")
from ROOT import *
#import scipy.integrate as integrate
#import scipy.special as special
import csv
from sklearn.metrics import roc_curve, auc
from sklearn.metrics import precision_recall_curve
from pathlib import Path
ROOT.gROOT.LoadMacro("AtlasStyle.C") 
SetAtlasStyle()

from BDT_all_1 import sr as s
print(s)
if "R" in s:
    header = [0,200,300,400,500,600,700,800,900,1000,1100,1200]
    name = "{}_v3_newpre".format(s)
else:
    header = [0,300,400,500,600,700,800,900,1000,1100,1200]
    name = "{}_v3_newpre".format(s)



#depth of tree:
d=7
#fp = open('AUC_v3_manysig.txt', 'w')

#name = "{}_v3_newpre".format(s)

tf = open('AUC_{}.csv'.format(name), 'w', newline='')
writer = csv.writer(tf, delimiter=',')
writer.writerow(header)
tf.close()

f = open('MaxSig_{}.csv'.format(name), 'w', newline='')
wr = csv.writer(f, delimiter=',')
wr.writerow(header)
f.close()

nwf = open('MaxSigNoWeight_{}.csv'.format(name), 'w', newline='')
nwwr = csv.writer(nwf, delimiter=',')
nwwr.writerow(header)
nwf.close()


pf = open('MaxSigPos_{}.csv'.format(name), 'w', newline='')
pwr = csv.writer(pf, delimiter=',')
pwr.writerow(header)
pf.close()

npf = open('MaxSigPosNoWeight_{}.csv'.format(name), 'w', newline='')
npwr = csv.writer(npf, delimiter=',')
npwr.writerow(header)
npf.close()

s0f = open('Sig0_{}.csv'.format(name), 'w', newline='')
s0wr = csv.writer(s0f, delimiter=',')
s0wr.writerow(header)
s0f.close()
s1f = open('Sig1_{}.csv'.format(name), 'w', newline='')
s1wr = csv.writer(s1f, delimiter=',')
s1wr.writerow(header)
s1f.close()
s2f = open('Sig2_{}.csv'.format(name), 'w', newline='')
s2wr = csv.writer(s2f, delimiter=',')
s2wr.writerow(header)
s2f.close()
s3f = open('Sig3_{}.csv'.format(name), 'w', newline='')
s3wr = csv.writer(s3f, delimiter=',')
s3wr.writerow(header)
s3f.close()
s4f = open('Sig4_{}.csv'.format(name), 'w', newline='')
s4wr = csv.writer(s4f, delimiter=',')
s4wr.writerow(header)
s4f.close()
s5f = open('Sig5_{}.csv'.format(name), 'w', newline='')
s5wr = csv.writer(s5f, delimiter=',')
s5wr.writerow(header)
s5f.close()
s6f = open('Sig6_{}.csv'.format(name), 'w', newline='')
s6wr = csv.writer(s6f, delimiter=',')
s6wr.writerow(header)
s6f.close()
s7f = open('Sig7_{}.csv'.format(name), 'w', newline='')
s7wr = csv.writer(s7f, delimiter=',')
s7wr.writerow(header)
s7f.close()

s8f = open('Sig8_{}.csv'.format(name), 'w', newline='')
s8wr = csv.writer(s8f, delimiter=',')
s8wr.writerow(header)
s8f.close()

s8f = open('Sig8_{}.csv'.format(name), 'w', newline='')
s8wr = csv.writer(s8f, delimiter=',')
s8wr.writerow(header)
s8f.close()

s9f = open('Sig9_{}.csv'.format(name), 'w', newline='')
s9wr = csv.writer(s9f, delimiter=',')
s9wr.writerow(header)
s9f.close()

s99f = open('Sig99_{}.csv'.format(name), 'w', newline='')
s99wr = csv.writer(s99f, delimiter=',')
s99wr.writerow(header)
s99f.close()

re0f = open('RawEvents0_{}.csv'.format(name), 'w', newline='')
re0wr = csv.writer(re0f, delimiter=',')
re0wr.writerow(header)
re0f.close()
re1f = open('RawEvents1_{}.csv'.format(name), 'w', newline='')
re1wr = csv.writer(re1f, delimiter=',')
re1wr.writerow(header)
re1f.close()
re2f = open('RawEvents2_{}.csv'.format(name), 'w', newline='')
re2wr = csv.writer(re2f, delimiter=',')
re2wr.writerow(header)
re2f.close()
re3f = open('RawEvents3_{}.csv'.format(name), 'w', newline='')
re3wr = csv.writer(re3f, delimiter=',')
re3wr.writerow(header)
re3f.close()
re4f = open('RawEvents4_{}.csv'.format(name), 'w', newline='')
re4wr = csv.writer(re4f, delimiter=',')
re4wr.writerow(header)
re4f.close()
re5f = open('RawEvents5_{}.csv'.format(name), 'w', newline='')
re5wr = csv.writer(re5f, delimiter=',')
re5wr.writerow(header)
re5f.close()
re6f = open('RawEvents6_{}.csv'.format(name), 'w', newline='')
re6wr = csv.writer(re6f, delimiter=',')
re6wr.writerow(header)
re6f.close()
re7f = open('RawEvents7_{}.csv'.format(name), 'w', newline='')
re7wr = csv.writer(re7f, delimiter=',')
re7wr.writerow(header)
re7f.close()

s8f = open('RawEvents8_{}.csv'.format(name), 'w', newline='')
s8wr = csv.writer(s8f, delimiter=',')
s8wr.writerow(header)
s8f.close()

s8f = open('RawEvents8_{}.csv'.format(name), 'w', newline='')
s8wr = csv.writer(s8f, delimiter=',')
s8wr.writerow(header)
s8f.close()

s9f = open('RawEvents9_{}.csv'.format(name), 'w', newline='')
s9wr = csv.writer(s9f, delimiter=',')
s9wr.writerow(header)
s9f.close()

s99f = open('RawEvents99_{}.csv'.format(name), 'w', newline='')
s99wr = csv.writer(s99f, delimiter=',')
s99wr.writerow(header)
s99f.close()


"""
c1 = TCanvas( 'c1', 'Dynamic Filling Example', 200, 10, 700, 500 )
c1.SetFillColor( 42 )
c1.GetFrame().SetFillColor( 21 )
c1.GetFrame().SetBorderSize( 6 )
c1.GetFrame().SetBorderMode( -1 )

sig_hist = TH2F('significance', 'significance for discriminant > 0.5', 11, 200, 1200, 4, 15, 45)
"""
print("start loop")


for eta in [45,35,25,15]:
    
    l=[eta]
    sl=[eta]
    psl=[eta]
    npsl=[eta]
    nsl=[eta]
    s99=[eta]
    re99=[eta]
    s9=[eta]
    re9=[eta]
    s0=[eta]
    re0=[eta]
    s1=[eta]
    re1=[eta]
    s2=[eta]
    re2=[eta]
    s3=[eta]
    re3=[eta]
    s4=[eta]
    re4=[eta]
    s5=[eta]
    re5=[eta]
    s6=[eta]
    re6=[eta]
    s7=[eta]
    re7=[eta]
    s8=[eta]
    re8=[eta]
    
    
    for m in [200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200]:
        if "R" in s:
            sig = "SU2R_{0}_{1}".format(eta,m)
        else:
            sig = "SU2L_{0}_{1}".format(eta,m)
        sigfile = Path("test_allx2_sig_{}.root".format(sig))
        if sigfile.exists() == False:
           continue
        print(sig)
        #y_true= load_data("test_allx_sig_{}.root".format(sig), "test_allxbg_*.root")
        
        #Transform all background test files into pandas dataframes and add a column with the name of the 
        df_test_ttbb = ROOT.RDataFrame("Events", "test_allxbg2_ttbb_*.root")
        npy_test_ttbb = df_test_ttbb.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_ttbb = pd.DataFrame(npy_test_ttbb)
        l_ttbb = ['ttbb']*len(panda_all_test_ttbb.Label)
        panda_all_test_ttbb['exactlabel']=l_ttbb

        df_test_ttallhad = ROOT.RDataFrame("Events", "test_allxbg2_ttallhad_*.root")
        npy_test_ttallhad = df_test_ttallhad.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_ttallhad = pd.DataFrame(npy_test_ttallhad)
        l_ttallhad = ['ttallhad']*len(panda_all_test_ttallhad.Label)
        panda_all_test_ttallhad['exactlabel']=l_ttallhad

        df_test_4top = ROOT.RDataFrame("Events", "test_allxbg2_4top_*.root")
        npy_test_4top = df_test_4top.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_4top = pd.DataFrame(npy_test_4top)
        l_4top = ['4top']*len(panda_all_test_4top.Label)
        panda_all_test_4top['exactlabel']=l_4top

        df_test_multiboson = ROOT.RDataFrame("Events", "test_allxbg2_multiboson_*.root")
        npy_test_multiboson = df_test_multiboson.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_multiboson = pd.DataFrame(npy_test_multiboson)
        l_multiboson = ['multiboson']*len(panda_all_test_multiboson.Label)
        panda_all_test_multiboson['exactlabel']=l_multiboson

        df_test_singletop = ROOT.RDataFrame("Events", "test_allxbg2_singletop_*.root")
        npy_test_singletop = df_test_singletop.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_singletop = pd.DataFrame(npy_test_singletop)
        l_singletop = ['singletop']*len(panda_all_test_singletop.Label)
        panda_all_test_singletop['exactlabel']=l_singletop

        df_test_ttnonallhad = ROOT.RDataFrame("Events", "test_allxbg2_ttnonallhad_*.root")
        npy_test_ttnonallhad = df_test_ttnonallhad.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_ttnonallhad = pd.DataFrame(npy_test_ttnonallhad)
        l_ttnonallhad = ['ttnonallhad']*len(panda_all_test_ttnonallhad.Label)
        panda_all_test_ttnonallhad['exactlabel']=l_ttnonallhad

        df_test_ttPlusX = ROOT.RDataFrame("Events", "test_allxbg2_ttPlusX_*.root")
        npy_test_ttPlusX = df_test_ttPlusX.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_ttPlusX = pd.DataFrame(npy_test_ttPlusX)
        l_ttPlusX = ['ttPlusX']*len(panda_all_test_ttPlusX.Label)
        panda_all_test_ttPlusX['exactlabel']=l_ttPlusX

        df_test_VJets = ROOT.RDataFrame("Events", "test_allxbg2_VJets_*.root")
        npy_test_VJets = df_test_VJets.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        panda_all_test_VJets = pd.DataFrame(npy_test_VJets)
        l_VJets = ['VJets']*len(panda_all_test_VJets.Label)
        panda_all_test_VJets['exactlabel']=l_VJets
        
        
    
        df_test = ROOT.RDataFrame("Events", "test_allxbg2_*.root")
        df_test_sig = ROOT.RDataFrame("Events", "test_allx2_sig_{}.root".format(sig))
        npy_test = df_test_sig.AsNumpy(columns=["BJets_70_N_c", "Jets_pt_0_c", "bb_m_for_minDeltaR_c", "Jets_N_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "deltaRLep2ndClosestBJet_c", "Label"])
        #npy_test = df_test.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        npy_test_sig = df_test_sig.AsNumpy(columns=["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c", "Label", "weight"])
        #npy_test = np.concatenate(npy_test, npy_test_sig)
        npy_label_bg = df_test.AsNumpy(columns=["Label"])
        npy_label_sig = df_test_sig.AsNumpy(columns=["Label"])
        y_true = np.hstack([npy_label_bg, npy_label_sig])
    
    
        #panda_all_test = pd.DataFrame(npy_test)
        panda_sig = pd.DataFrame(npy_test_sig)
        l_sig = ['sig']*len(panda_sig.Label)
        panda_sig['exactlabel']=l_sig
        panda_all_test = pd.concat([panda_all_test_ttbb, panda_all_test_ttallhad, panda_all_test_4top, panda_all_test_multiboson, panda_all_test_singletop, panda_all_test_ttnonallhad, panda_all_test_ttPlusX, panda_all_test_VJets, panda_sig])
        panda_all_test = panda_all_test.sample(frac=1)
        print(panda_all_test.shape)


        #panda_bg_train = panda_bg_train1[:40000]
        #panda_all_test = panda_all_test[:500000]





        panda_test = panda_all_test.sample(frac=1)



        #add 'Label' as category to pandas frames
        #panda_train['Label'] = panda_train.Label.astype('category')
        #print('Fraction signal in training data: {}'.format(len(panda_train[panda_train.Label == 1])/(float)(len(panda_train[panda_train.Label == 1]) + len(panda_train[panda_train.Label == 0]))))

        panda_test['Label'] = panda_test.Label.astype('category')


        #print('Fraction signal in test data: {}'.format(len(panda_test[panda_test.Label == 1])/(float)(len(panda_test[panda_test.Label == 1]) + len(panda_test[panda_test.Label == 0]))))


        print("length of panda test: ",len(panda_test))

    
        feature_names_test = panda_test.columns[0:8]
        #print(panda_test[feature_names])
        test = xgb.DMatrix(data=panda_test[feature_names_test],label=panda_test.Label.cat.codes, feature_names=feature_names_test)
    


        model = xgb.Booster()

        model.load_model("{}_model_v3_p2.json".format(s))
    

        predictions = model.predict(test)
        #p = open('predictions_{}.txt'.format(sig), 'w')
        #p.write(predictions)
    
        #add discriminant to pandas dataframe
        panda_test['discriminant']=predictions
        panda_test_s = panda_test[panda_test.exactlabel=='sig']
        
        #create dictionary from pandas dataframe
        data_s = {key: panda_test_s[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
    
        #convert column Label from int to float
        data_s["Label"] = data_s["Label"].astype(np.float32)
    
        #create and save RDataframe from dictionary
        rdf_disc = ROOT.RDF.MakeNumpyDataFrame(data_s)
        rdf_disc.Snapshot("Events", "test_{0}_{1}_withDisc.root".format(name,sig))
        
        
        # plot discriminant with weights
        plt.figure();
        plt.hist(panda_test.discriminant[panda_test.Label == 0],weights=panda_test.weight[panda_test.Label == 0], bins=np.linspace(0,1,200), density=False, histtype='step',color='midnightblue',label='background');
        plt.hist(panda_test.discriminant[panda_test.Label == 1],weights=panda_test.weight[panda_test.Label == 1], bins=np.linspace(0,1,200), density=True, histtype='step',color='firebrick',label='signal');
        # make the plot readable
        plt.xlabel('Discriminant',fontsize=12);
        plt.ylabel('Events',fontsize=12);
        plt.legend(frameon=False);
        plt.savefig("discriminant_weights_{1}_{0}.png".format(sig,name))
        plt.close()
        
        plt.figure();
        plt.hist(panda_test.discriminant[panda_test.Label == 0],weights=panda_test.weight[panda_test.Label == 0], bins=np.linspace(0,1,200), density=False, histtype='step',color='midnightblue',label='background');
        plt.hist(panda_test.discriminant[panda_test.Label == 1],weights=panda_test.weight[panda_test.Label == 1], bins=np.linspace(0,1,200), density=False, histtype='step',color='firebrick',label='signal');
        plt.yscale('log')
        # make the plot readable
        plt.xlabel('Discriminant',fontsize=12);
        plt.ylabel('Events',fontsize=12);
        plt.legend(frameon=False);
        plt.savefig("discriminant_weights_{1}_{0}_logsc.png".format(sig,name))
    
        plt.close()
        """
        # plot discriminant
        plt.figure();
        plt.hist(predictions[~test.get_label().astype(bool)],bins=np.linspace(0,1,200), density=True,         histtype='step',color='midnightblue',label='background');
        plt.hist(predictions[(test.get_label().astype(bool))],bins=np.linspace(0,1,200), density=True,         histtype='step',color='firebrick',label='signal');
        # make the plot readable
        plt.xlabel('Discriminant',fontsize=12);
        plt.ylabel('Events',fontsize=12);
        plt.legend(frameon=False);
        plt.savefig("discriminant_{1}_{0}.png".format(sig,name))
        plt.close()
    
    
        plt.figure();
        plt.hist(predictions[~test.get_label().astype(bool)],bins=np.linspace(0,1,200), density=True,         histtype='step',color='midnightblue',label='background');
        plt.hist(predictions[(test.get_label().astype(bool))],bins=np.linspace(0,1,200), density=True,         histtype='step',color='firebrick',label='signal');
        plt.yscale('log')
        # make the plot readable
        plt.xlabel('Discriminant',fontsize=12);
        plt.ylabel('Events',fontsize=12);
        plt.legend(frameon=False);
        plt.savefig("discriminant_{1}_{0}_logsc.png".format(s,sig,name))
    
        plt.close()
        """
        fpr, tpr, _ = roc_curve(panda_test.Label, panda_test.discriminant)
        score = auc(fpr, tpr)
        plt.figure();
        plt.plot(fpr,tpr)
        plt.xlabel("False-Positive rate")
        plt.ylabel("True-Positive rate")
        plt.savefig("ROC_{0}_{1}.jpg".format(sig,name))
        plt.close()
        """ 
        c = ROOT.TCanvas("roc", "", 600, 600)
        g = ROOT.TGraph(len(fpr), fpr, tpr)
        g.SetTitle("AUC = {:.2f}".format(score))
        g.SetLineWidth(3)
        g.SetLineColor(ROOT.kRed)
        g.Draw("AC")
        g.GetXaxis().SetRangeUser(0, 1)
        g.GetYaxis().SetRangeUser(0, 1)
        g.GetXaxis().SetTitle("False-positive rate")
        g.GetYaxis().SetTitle("True-positive rate")
        c.Draw()
        c.SaveAs("ROC_{0}_{1}.jpg".format(sig,name))
        c.Close()
        """
        cuts = np.linspace(0,1,1000);
        #cuts=np.linspace(0,0.1,500)+np.linspace(0.1,1,50)
        nsignal = np.zeros(len(cuts));
        nbackground = np.zeros(len(cuts));
        wsignal = np.zeros(len(cuts));
        wbackground = np.zeros(len(cuts));
        significance = np.zeros(len(cuts));
        sig_nw = np.zeros(len(cuts));
        for i,cut in enumerate(cuts):
            nsignal[i] = len(np.where(predictions[test.get_label().astype(bool)] > cut)[0]); #true positive
            nbackground[i] = len(np.where(predictions[~(test.get_label().astype(bool))] > cut)[0]); #false positive
            wsignal[i] = sum(panda_test.weight[(panda_test.Label == 1) & (panda_test.discriminant > cut)]);
            wbackground[i] = sum(panda_test.weight[(panda_test.Label == 0) & (panda_test.discriminant > cut)]);
            significance[i] = wsignal[i]/np.sqrt(wsignal[i] + wbackground[i])
            sig_nw[i] = nsignal[i]/np.sqrt(nsignal[i]+nbackground[i])
        #print("nsignal: ", nsignal[:-1])
        #print("nbackground: ", nbackground[:-1])
            
        # plot efficiency vs. purity (PRC curve)
        plt.figure();
        plt.plot(nsignal/len(panda_test[panda_test.Label == 1]),nsignal/(nsignal + nbackground),'o-',color='blueviolet');
        
        np.seterr(invalid='ignore')
        
        #fpr, tpr, _ = roc_curve(signal, panda_test[panda_test.Label==0])
        
        y = nsignal/len(panda_test[panda_test.Label == 1])
        x = nsignal/(nsignal + nbackground)
        score = np.sum(y)/len(y)
    
        
        plt.title("{}".format(score))
        plt.xlabel('Efficiency',fontsize=12);
        plt.ylabel('Purity',fontsize=12);
        plt.savefig("ROC_all_{0}_{1}_{2}.png".format(s,sig,name))
        plt.close()
    
        
        no_skill = len(panda_test[panda_test.Label==1]) / len(panda_test)
        plt.plot([0, 1], [no_skill, no_skill], linestyle='--', label='No Skill')
        
        precision, recall, _ = precision_recall_curve(panda_test.Label, panda_test.discriminant)
        auc_score = auc(recall, precision)
        
        plt.plot(recall, precision, marker='.', label='Logistic')
        plt.xlabel('Recall (Efficiency)')
        plt.ylabel('Precision (Purity)')
        plt.title('{}'.format(auc_score)) 
        plt.legend()
        plt.savefig("PRC_{0}_{1}.png".format(sig,name))
        plt.close()
        
        #significance vs discriminant
        plt.figure();
        plt.plot(cuts,nsignal/np.sqrt(nsignal + nbackground),'o-',color='blue')
        plt.xlabel('Cut on discriminant', fontsize=12)
        plt.ylabel('s/sqrt(s+b) (significance)', fontsize=12)
        plt.savefig("Significance_{0}_{1}.png".format(sig,name))
        plt.close()
        
        plt.figure();
        plt.plot(cuts,wsignal/np.sqrt(wsignal + wbackground),'o-',color='blue')
        plt.xlabel('Cut on discriminant', fontsize=12)
        plt.ylabel('s/sqrt(s+b) (significance)', fontsize=12)
        plt.savefig("Significance_{0}_{1}_weights.png".format(sig,name))
        plt.close()
        
        #purity vs discriminant
        plt.figure();
        plt.plot(cuts,nsignal/(nsignal + nbackground),'o-',color='blue')
        plt.xlabel('Cut on discriminant', fontsize=12)
        plt.ylabel('s/(s+b) (significance)', fontsize=12)
        plt.savefig("Purity_{0}_{1}.png".format(sig, name))
        plt.close()
        
        plt.figure();
        plt.plot(cuts,wsignal/(wsignal + wbackground),'o-',color='blue')
        plt.xlabel('Cut on discriminant', fontsize=12)
        plt.ylabel('s/(s+b) (significance)', fontsize=12)
        plt.savefig("Purity_{0}_{1}_weights.png".format(sig, name))
        plt.close()
        
        l.append(auc_score)
        sl.append(max(significance))
        max_sig = max(significance)
        index = np.where(significance==max_sig)
        print(index)
        psl.append(cuts[index])
        max_sig_nw = max(sig_nw)
        ind_nw = np.where(sig_nw==max_sig_nw)
        npsl.append(cuts[ind_nw])
        nsl.append(max_sig_nw)
        
        s0.append(significance[0])
        s1.append(significance[100])
        s2.append(significance[200])
        s3.append(significance[300])
        s4.append(significance[400])
        s5.append(significance[500])
        s6.append(significance[600])
        s7.append(significance[700])
        s8.append(significance[800])
        s9.append(significance[900])
        s99.append(significance[989])
        re0.append(nsignal[0])
        re1.append(nsignal[100])
        re2.append(nsignal[200])
        re3.append(nsignal[300])
        re4.append(nsignal[400])
        re5.append(nsignal[500])
        re6.append(nsignal[600])
        re7.append(nsignal[700])
        re8.append(nsignal[800])
        re9.append(nsignal[900])
        re99.append(nsignal[989])
        #sig_hist.Fill(m, eta)
        #sig_hist.SetBinContent(m,eta,significance[500])
    print(sl)
    
    

    
    tf1 = open('AUC_{}.csv'.format(name), 'a', newline='')
    writer1 = csv.writer(tf1, delimiter=',')
    writer1.writerow(l)
    tf1.close()
    
    f1 = open('MaxSig_{}.csv'.format(name), 'a', newline='')
    wr1 = csv.writer(f1, delimiter=',')
    wr1.writerow(sl)
    f1.close()
    
    pf1 = open('MaxSigPos_{}.csv'.format(name), 'a', newline='')
    pwr1 = csv.writer(pf1, delimiter=',')
    pwr1.writerow(psl)
    pf1.close()
    
    nwf1 = open('MaxSigNoWeight_{}.csv'.format(name), 'a', newline='')
    nwwr1 = csv.writer(nwf1, delimiter=',')
    nwwr1.writerow(nsl)
    nwf1.close()
    
    npf1 = open('MaxSigPosNoWeight_{}.csv'.format(name), 'a', newline='')
    npwr1 = csv.writer(npf1, delimiter=',')
    npwr1.writerow(npsl)
    npf1.close()
    
    s0f1 = open('Sig0_{}.csv'.format(name), 'a', newline='')
    s0wr1 = csv.writer(s0f1, delimiter=',')
    s0wr1.writerow(s0)
    s0f1.close()
    s1f1 = open('Sig1_{}.csv'.format(name), 'a', newline='')
    s1wr1 = csv.writer(s1f1, delimiter=',')
    s1wr1.writerow(s1)
    s1f1.close()
    s2f1 = open('Sig2_{}.csv'.format(name), 'a', newline='')
    s2wr1 = csv.writer(s2f1, delimiter=',')
    s2wr1.writerow(s2)
    s2f1.close()
    s3f1 = open('Sig3_{}.csv'.format(name), 'a', newline='')
    s3wr1 = csv.writer(s3f1, delimiter=',')
    s3wr1.writerow(s3)
    s3f1.close()
    s4f1 = open('Sig4_{}.csv'.format(name), 'a', newline='')
    s4wr1 = csv.writer(s4f1, delimiter=',')
    s4wr1.writerow(s4)
    s4f1.close()
    s5f1 = open('Sig5_{}.csv'.format(name), 'a', newline='')
    s5wr1 = csv.writer(s5f1, delimiter=',')
    s5wr1.writerow(s5)
    s5f1.close()
    s6f1 = open('Sig6_{}.csv'.format(name), 'a', newline='')
    s6wr1 = csv.writer(s6f1, delimiter=',')
    s6wr1.writerow(s6)
    s6f1.close()
    s7f1 = open('Sig7_{}.csv'.format(name), 'a', newline='')
    s7wr1 = csv.writer(s7f1, delimiter=',')
    s7wr1.writerow(s7)
    s7f1.close()
    s8f1 = open('Sig8_{}.csv'.format(name), 'a', newline='')
    s8wr1 = csv.writer(s8f1, delimiter=',')
    s8wr1.writerow(s8)
    s8f1.close()
    s9f1 = open('Sig9_{}.csv'.format(name), 'a', newline='')
    s9wr1 = csv.writer(s9f1, delimiter=',')
    s9wr1.writerow(s9)
    s9f1.close() 
    s99f1 = open('Sig99_{}.csv'.format(name), 'a', newline='')
    s99wr1 = csv.writer(s99f1, delimiter=',')
    s99wr1.writerow(s99)
    s99f1.close()
    
    re0f1 = open('RawEvents0_{}.csv'.format(name), 'a', newline='')
    re0wr1 = csv.writer(re0f1, delimiter=',')
    re0wr1.writerow(re0)
    re0f1.close()
    re1f1 = open('RawEvents1_{}.csv'.format(name), 'a', newline='')
    re1wr1 = csv.writer(re1f1, delimiter=',')
    re1wr1.writerow(re1)
    re1f1.close()
    re2f1 = open('RawEvents2_{}.csv'.format(name), 'a', newline='')
    re2wr1 = csv.writer(re2f1, delimiter=',')
    re2wr1.writerow(re2)
    re2f1.close()
    re3f1 = open('RawEvents3_{}.csv'.format(name), 'a', newline='')
    re3wr1 = csv.writer(re3f1, delimiter=',')
    re3wr1.writerow(re3)
    re3f1.close()
    re4f1 = open('RawEvents4_{}.csv'.format(name), 'a', newline='')
    re4wr1 = csv.writer(re4f1, delimiter=',')
    re4wr1.writerow(re4)
    re4f1.close()
    re5f1 = open('RawEvents5_{}.csv'.format(name), 'a', newline='')
    re5wr1 = csv.writer(re5f1, delimiter=',')
    re5wr1.writerow(re5)
    re5f1.close()
    re6f1 = open('RawEvents6_{}.csv'.format(name), 'a', newline='')
    re6wr1 = csv.writer(re6f1, delimiter=',')
    re6wr1.writerow(re6)
    re6f1.close()
    re7f1 = open('RawEvents7_{}.csv'.format(name), 'a', newline='')
    re7wr1 = csv.writer(re7f1, delimiter=',')
    re7wr1.writerow(re7)
    re7f1.close()
    re8f1 = open('RawEvents8_{}.csv'.format(name), 'a', newline='')
    re8wr1 = csv.writer(re8f1, delimiter=',')
    re8wr1.writerow(re8)
    re8f1.close()
    re9f1 = open('RawEvents9_{}.csv'.format(name), 'a', newline='')
    re9wr1 = csv.writer(re9f1, delimiter=',')
    re9wr1.writerow(re9)
    re9f1.close() 
    re99f1 = open('RawEvents99_{}.csv'.format(name), 'a', newline='')
    re99wr1 = csv.writer(re99f1, delimiter=',')
    re99wr1.writerow(re99)
    re99f1.close()

#sig_hist.Draw("HIST")
#c1.Update()
#c1.SaveAs("significance_2D_{}.pdf".format(name))
#c1.Close()

#Save background files with discriminant
panda_test_ttbb = panda_test[panda_test.exactlabel=='ttbb']
data_ttbb = {key: panda_test_ttbb[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_ttbb["Label"] = data_ttbb["Label"].astype(np.float32)
rdf_disc_ttbb = ROOT.RDF.MakeNumpyDataFrame(data_ttbb)
rdf_disc_ttbb.Snapshot("Events", "test_{}_ttbb_withDisc.root".format(name))

panda_test_ttallhad = panda_test[panda_test.exactlabel=='ttallhad']
data_ttallhad = {key: panda_test_ttallhad[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_ttallhad["Label"] = data_ttallhad["Label"].astype(np.float32)
rdf_disc_ttallhad = ROOT.RDF.MakeNumpyDataFrame(data_ttallhad)
rdf_disc_ttallhad.Snapshot("Events", "test_{}_ttallhad_withDisc.root".format(name))

panda_test_ttnonallhad = panda_test[panda_test.exactlabel=='ttnonallhad']
data_ttnonallhad = {key: panda_test_ttnonallhad[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_ttnonallhad["Label"] = data_ttnonallhad["Label"].astype(np.float32)
rdf_disc_ttnonallhad = ROOT.RDF.MakeNumpyDataFrame(data_ttnonallhad)
rdf_disc_ttnonallhad.Snapshot("Events", "test_{}_ttnonallhad_withDisc.root".format(name))

panda_test_4top = panda_test[panda_test.exactlabel=='4top']
data_4top = {key: panda_test_4top[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_4top["Label"] = data_4top["Label"].astype(np.float32)
rdf_disc_4top = ROOT.RDF.MakeNumpyDataFrame(data_4top)
rdf_disc_4top.Snapshot("Events", "test_{}_4top_withDisc.root".format(name))

panda_test_multiboson = panda_test[panda_test.exactlabel=='multiboson']
data_multiboson = {key: panda_test_multiboson[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_multiboson["Label"] = data_multiboson["Label"].astype(np.float32)
rdf_disc_multiboson = ROOT.RDF.MakeNumpyDataFrame(data_multiboson)
rdf_disc_multiboson.Snapshot("Events", "test_{}_multiboson_withDisc.root".format(name))

panda_test_singletop = panda_test[panda_test.exactlabel=='singletop']
data_singletop = {key: panda_test_singletop[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_singletop["Label"] = data_singletop["Label"].astype(np.float32)
rdf_disc_singletop = ROOT.RDF.MakeNumpyDataFrame(data_singletop)
rdf_disc_singletop.Snapshot("Events", "test_{}_singletop_withDisc.root".format(name))

panda_test_ttPlusX = panda_test[panda_test.exactlabel=='ttPlusX']
data_ttPlusX = {key: panda_test_ttPlusX[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_ttPlusX["Label"] = data_ttPlusX["Label"].astype(np.float32)
rdf_disc_ttPlusX = ROOT.RDF.MakeNumpyDataFrame(data_ttPlusX)
rdf_disc_ttPlusX.Snapshot("Events", "test_{}_ttPlusX_withDisc.root".format(name))

panda_test_VJets = panda_test[panda_test.exactlabel=='VJets']
data_VJets = {key: panda_test_VJets[key].values for key in ["Jets_N_c", "BJets_70_N_c", "bb_m_for_minDeltaR_c", "mT_c", "Leptons_pt_c","HT_pt_c","deltaRBJet1Lep_c", "LJet_m_plus_RCJet_m_12_c","Label", "discriminant", "weight"]}
data_VJets["Label"] = data_VJets["Label"].astype(np.float32)
rdf_disc_VJets = ROOT.RDF.MakeNumpyDataFrame(data_VJets)
rdf_disc_VJets.Snapshot("Events", "test_{}_VJets_withDisc.root".format(name))

#xgb.plot_tree(model)

#fig = plt.gcf()
#fig.set_size_inches(100, 100)
#fig.savefig('tree_v3_manysig_{}.png'.format(s))    

print("finished succesfully")
